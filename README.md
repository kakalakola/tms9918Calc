# Texas Instruments TMS9918 VDC Register Calculator
(I still need to find a better name)

Version 0.85
 - Added option to hide unused registers
 - Cleaned up code, slightly

Version 0.8
 - Initial Upload
 - Support for:
  - Texas Instruments TMS99918
  - SEGA 315-5124/315-5246 VDP (Used in Mark III/Master System/Master System II and Game Gear)
  - SEGA 315-5315 VDP (Used in Mega Drive/Genesis)
  - SEGA 315-5818 VDP (Used in 32X)